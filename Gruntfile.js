module.exports = function(grunt) {

    require('time-grunt')(grunt);

    //-------------------------------------------------------------------
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssFolder:'css',
        runTarget: 'home.html',
        cssmin: {
            options: {
            },

            combine: {
                files: [{
                    expand: true,
                    cwd: '<%= cssFolder %>',
                    src: ['*.css'],
                    dest: 'dest/css',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['css/*.less'],
                tasks: ['less','cssmin'],
                options: {
                    spawn: false,
                    livereload: true
                },
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './**/*.css',
                        './**/*.html',
                        './**/*.js',
                        '!node_modules/**/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    port:3061,
                    server: {
                        baseDir: "./",
                        index: "<%= runTarget %>"
                    }
                }
            }
        },
        clean: {
            css: ["css/*.css"],
        },
        less: {
            development: {
                options: {
                    paths: ["css"],
                    cleancss: true
                },
                files: {
                    "<%= cssFolder %>/home_style.css": 
                      [ "<%= cssFolder %>/config.less",
                        "<%= cssFolder %>/home_style.less", 
                        "<%= cssFolder %>/dropdownMenu.less"],
                    "<%= cssFolder %>/checkout.css": 
                      [ "<%= cssFolder %>/config.less",
                        "<%= cssFolder %>/checkout.less",
                        "<%= cssFolder %>/dropdownMenu.less"],
                    "<%= cssFolder %>/thank_you.css":
                      [ "<%= cssFolder %>/config.less",
                          "<%= cssFolder %>/thank_you.less"],
                }
            }
        }
    });
    //-------------------------------------------------------------------
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-less');
    //-------------------------------------------------------------------

    grunt.registerTask('dev', ['browserSync', 'watch']);
    grunt.registerTask('build', ['less', 'cssmin','clean']);

    grunt.registerTask('run', 'Start a custom web server', function(mode) {
        var target;
        switch (mode){
            case 'checkout':
                target='checkout.html';
                break;
            case 'thank_you':
                target='thank_you.html';
                break;
            default:
                target='home.html';
        }
        grunt.config.set('runTarget', target);
        // grunt.task.run('build');
        grunt.task.run('dev');
    });

};
