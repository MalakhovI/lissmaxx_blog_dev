/**
 * Created by elenak on 23.11.16.
 */
function initialize() {
    var mapCanvas = document.getElementById('map_canvas');
    var mapOptions = {
        center: new google.maps.LatLng(40.730610, -73.935242),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);

    var markers = [],
        myPlaces = [];
    //array of marked places
    myPlaces.push(new Place('New York', 40.730610, -73.935242, 'big apple'));

    //adding markers for places from array
    for (var i = 0, n = myPlaces.length; i < n; i++) {
        var marker = new google.maps.Marker({
            //position
            position: new google.maps.LatLng(myPlaces[i].latitude, myPlaces[i].longitude),
            map: map,
            //on mouse over the marker
            title: myPlaces[i].name
        });
        //popup when click on marker
        var infowindow = new google.maps.InfoWindow({
            content: '<h1>' + myPlaces[i].name + '</h1><br/>' + myPlaces[i].description
        });
        //closing popup to marker on the map
        makeInfoWindowEvent(map, infowindow, marker);
        markers.push(marker);
    }
}
function makeInfoWindowEvent(map, infowindow, marker) {
    //closing click event to marker
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
}

function Place(name, latitude, longitude, description){
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.description = description;
}

google.maps.event.addDomListener(window, 'load', initialize);
